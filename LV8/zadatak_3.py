
from tensorflow.keras.preprocessing import image
model = load_model('/content/ FCA ')
model . summary()


img = image.load_img('/content/test3.png',
                     color_mode="grayscale", target_size=(28, 28))
img_arr = np.array(img)
img_arr = img_arr.astype("float32")/255
img_arr = np.expand_dims(img_arr, -1)
img_arr = np.reshape(img_arr, (1, 784))

result = model.predict(img_arr)
print(result)
predicted_class = np.argmax(result)

print("Predicted digit:", predicted_class)
