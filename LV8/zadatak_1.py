import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa

fig, axs = plt.subplots(1, 5,)
for i in range(5):
    axs[i].imshow(x_train[i], cmap='gray')
    axs[i].set_title(f"Label: {y_train[i]}")
plt.show()


# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)
x_train_s = np.reshape(x_train_s,(60000,784))
x_test_s = np.reshape(x_test_s,(10000,784))

# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu

model = keras.models.Sequential([
  keras.layers.Dense(100,input_shape=(784, ),activation='relu'),
  keras.layers.Dense(50, activation='relu'),
  keras.layers.Dense(10, activation='softmax')
])

model.summary()


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()

model.compile( loss ="categorical_crossentropy" ,
optimizer ="adam",
metrics =["accuracy" ,])



# TODO: provedi ucenje mreze
epochs = 20
history = model.fit(x_train_s, y_train_s, batch_size=32, epochs=epochs, validation_split=0.2)
predictions = model.predict(x_test_s)

# TODO: Prikazi test accuracy i matricu zabune
score = model.evaluate( x_test_s , y_test_s , verbose =0)
print('score:', score)

# Predict test labels
y_pred =np.argmax(predictions, axis=1)

# Calculate confusion matrix
conf_mat = confusion_matrix(y_test, y_pred)
print('Confusion matrix:')
print(conf_mat)


# TODO: spremi model

model . save (" FCN /")
del model