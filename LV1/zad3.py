numbers = []

while(True):
    word = input()
    if(word == "Done"):
        break
    try:
        numbers.append(int(word))
    except ValueError:
        print("NaN")

if(len(numbers) != 0):
    print("Broj brojeva: " + str(len(numbers)))
    print("Sredina: "+ str(round(sum(numbers) / len(numbers),2)))
    print("Max: " + str(max(numbers)))
    print("Min: " + str(min(numbers)))
    print("Sorted: " +str(sorted(numbers)))
else:
    print("No numbers to output")