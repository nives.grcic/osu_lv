
hamTextCounter = 0
hamWordCounter = 0

spamTextCounter = 0
spamWordCounter = 0

ExclamationCounter = 0

with open("SMSSpamCollection.txt") as file:
    for line in file:
            words = line.split()
            if(words[0] == "ham"):
                hamTextCounter +=1
                hamWordCounter+=len(words) -1

            elif(words[0] == "spam"):
                spamTextCounter +=1
                spamWordCounter+=len(words) -1

                lastWord = words[len(words)-1]
                lastSymbol = lastWord[len(lastWord)-1]
                if(lastSymbol == "!"):
                    ExclamationCounter+=1
    file.close()

print("Average number of spam " + str(round(spamWordCounter/spamTextCounter)))
print("Average number of ham " + str( round(hamWordCounter/hamTextCounter)))
print("Ends with exclamation mark " + str(ExclamationCounter))
    
