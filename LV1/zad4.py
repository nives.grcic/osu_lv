
words = {}
with open("song.txt") as file:
    for line in file :
        for word in line.split():
            if(words.get(word)):
                words[word]+=1
            else:
                words[word] = 1

numOfWords = 0
for i in words:
    if(words[i] == 1):
        numOfWords+=1
        print(i)
print("Total number of words that appear once: "+ str(numOfWords))
file.close()
