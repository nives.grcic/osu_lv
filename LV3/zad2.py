import pandas as pd

data = pd.read_csv('data_C02_emission.csv')
import matplotlib.pyplot as plt

plt.figure ()
data['CO2 Emissions (g/km)'].plot(kind='hist', bins = 20)
plt.show()
data.plot.scatter(x='Fuel Consumption City (L/100km)',y='CO2 Emissions (g/km)')
plt.show()