import pandas as pd

data = pd.read_csv('data_C02_emission.csv')
print("-------a podzadatak--------")
print("BROJ MJERENJA")
print(len(data))
print("TIPOVI VELICINA")
print(data.info)
print("IZOSTALE I DUPLICIRANE VRIJEDNOSTI")
#provjera koliko je izostalih vrijednosti po svakom stupcu DataFramea
print(data.isnull ().sum())
#brisanje redova gdje barem vrijednost jedne velicine nedostaje
data.dropna(axis=0)
#brisanje stupaca gdje barem jedna vrijednost nedostaje
data.dropna(axis=1)
#brisanje dupliciranih redova
data.drop_duplicates ()
#kada se obrisu pojedini redovi potrebno je resetirati indekse retka
data = data.reset_index(drop =True)

print("KONVERTIRANJE KATEGORICKIH VELICINA")
#konvertiranje kategorickih velicina u tip category
#data.astype['Make':'category','Model':'category']


print("-------b podzadatak--------")
print(data.sort_values(by=['Fuel Consumption City (L/100km)']).head(3)[['Make','Model','Fuel Consumption City (L/100km)']])



print("-------c podzadatak--------")
print("MOTOR IZMEDU 2.5 I 3.5")
engineRestrictedData = data[(data['Engine Size (L)'] >=2.5) & data["Engine Size (L)"]<= 3.5]
print(len(engineRestrictedData))

print("PROSJECNO CO2")
print(engineRestrictedData['CO2 Emissions (g/km)'])


print("-------d podzadatak--------")
Audi = data[(data['Make'] == "Audi")]
print("BROJ AUDI AUTA")
print(len(Audi))
print("CO2 EMISIJA AUDI AUTA S $ CILINDRA")
print(Audi[(Audi['Cylinders'] == 4)]['CO2 Emissions (g/km)'])


print("-------e podzadatak--------")
cylinders = data.groupby('Cylinders')
print(cylinders.size())
print(cylinders['CO2 Emissions (g/km)'].mean())


print("-------f podzadatak--------")
diesel_cars = data[(data['Fuel Type'] == 'D')]
benz_cars = data[(data['Fuel Type'] == 'X')]

print(diesel_cars['Fuel Consumption City (L/100km)'].mean())
print(benz_cars['Fuel Consumption City (L/100km)'].mean())
print(diesel_cars['Fuel Consumption City (L/100km)'].median())
print(benz_cars['Fuel Consumption City (L/100km)'].median())


print("-------g podzadatak--------")

cylinder4_and_diesel = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]
print(cylinder4_and_diesel.sort_values(by = 'Fuel Consumption City (L/100km)').tail(1)[['Make','Model','Fuel Consumption City (L/100km)']])


print("-------h podzadatak--------")

manual = data[(data['Transmission'].str.contains("M",case = False))]
manual = manual.reset_index(drop = True)
print(len(manual))


print("-------i podzadatak--------")
print(data.corr(numeric_only=True))