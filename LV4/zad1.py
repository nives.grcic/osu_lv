
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn . preprocessing import MinMaxScaler

print("------------a------------")
data = pd.read_csv('data_C02_emission.csv')
data = data.drop(["Make","Model"], axis = 1)

input_var = ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)','Fuel Type']
output_var = ["CO2 Emissions (g/km)"]

x = data[input_var]
y = data[output_var]

from sklearn . preprocessing import OneHotEncoder
ohe = OneHotEncoder ()
X_encoded =pd.DataFrame( ohe.fit_transform ( data [["Fuel Type"]]).toarray())

x = pd.concat([x.drop("Fuel Type", axis=1), X_encoded], axis=1)


X_train, X_test, y_train, y_test = train_test_split(x,y, test_size=0.2, random_state=1)

print("------------b------------")
plt.scatter(X_train["Fuel Consumption City (L/100km)"], y_train, color='blue', label='Training Data')
plt.scatter(X_test["Fuel Consumption City (L/100km)"], y_test, color='red', label='Testing Data')
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel('CO2 Emissions(g/km)')
plt.legend()
plt.show()
print("------------c------------")

# min - max skaliranje
sc = StandardScaler ()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

plt.subplot(1,2,1)
plt.hist(X_train["Fuel Consumption City (L/100km)"], bins=15, alpha=0.5, label='Before Scaling')
plt.title("Before Scaling")
plt.subplot(1,2,2)
plt.hist(X_train_n[:,3], bins=15, alpha=0.5)
plt.title("After Scaling")
plt.show()



print("------------d------------")  
import sklearn.linear_model as lm
linearModel = lm.LinearRegression ()
linearModel.fit ( X_train_n , y_train )

print("Intercept:", linearModel.intercept_)
print("Coefficients:", linearModel.coef_)

print("------------e------------")
from sklearn . metrics import mean_absolute_error
# predikcija izlazne velicine na skupu podataka za testiranje
y_test_p = linearModel . predict ( X_test_n )


plt.scatter(X_test["Fuel Consumption City (L/100km)"], y_test_p, color='blue', label='Training Data')
plt.scatter(X_test["Fuel Consumption City (L/100km)"], y_test, color='red', label='Testing Data')
plt.xlabel("Fuel Consumption City (L/100km)")
plt.ylabel('CO2 Emissions(g/km)')
plt.legend()
plt.show()


print("------------f------------")
from sklearn import metrics
from sklearn.metrics import mean_absolute_percentage_error
# evaluacija modela na skupu podataka za testiranje pomocu MAE
MAE = mean_absolute_error ( y_test , y_test_p )
MSE = metrics.mean_squared_error(y_test, y_test_p)
RMSE = np.sqrt(MSE)
MAPE = mean_absolute_percentage_error(y_test,y_test_p)
R2 = metrics.r2_score(y_test, y_test_p)
print(MAE,MSE,RMSE,MAPE,R2)
print("------------g------------")

print("50/50 split: 8.340541831221916 235.3122663008884 15.33989133927905 0.030350675795660635 0.9389869301569862")
print("80/20 split: 8.18449505021557 257.2002271349327 16.037463238771046 0.029892029481725053 0.9364108013367262")
print("20/80 split: 9.114280808885496 242.60688361821846 15.57584295048645 0.03338315399631689 0.9390570267535682")