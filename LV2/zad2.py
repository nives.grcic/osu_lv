import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt("data.csv",delimiter=',',skiprows=1)

#shape prikazuje dimanzije matrice u obliku (stupac,redak)
print(np.shape(data))

x = data[:,1] #visina
y=data[:,2] #masa
plt.scatter(x,y)
plt.title('Height-Weight ratio')
plt.show()


data50 = data[::50]

x50 = data50[:,1]
y50 = data50[:,2]
plt.scatter(x50,y50)
plt.title('Height-Weight ratio and info')
plt.show()


print("Max Height: " + str(x.max()))
print("Min Height: " + str(x.min()))
print("Mean Height: " + str(x.mean()))
print("---------------------")

women = (data[:,0] == 0)
men = (data[:,0] == 1)


print("Max Height Women: " + str(data[women,1].max()))
print("Min Height Women: " + str(data[women,1].min()))
print("Mean Height Women: " + str(data[women,1].mean()))
print("---------------------------")
print("Max Height Men: " + str(data[men,1].max()))
print("Min Height Men: " + str(data[men,1].min()))
print("Mean Height Men: " + str(data[men,1].mean()))