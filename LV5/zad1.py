
from sklearn.metrics import accuracy_score, recall_score, precision_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.linear_model import LogisticRegression
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                           random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=5)


plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train,
            cmap='bwr', label='Training data')
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test,
            cmap='bwr', marker='x', label='Testing data')
plt.xlabel('X1')
plt.ylabel('X2')
plt.legend()
plt.show()


model = LogisticRegression(random_state=5)
model.fit(X_train, y_train)

model.coef_

# Retrieve the model parameters.
b = model.intercept_[0]
w1, w2 = model.coef_.T
# Calculate the intercept and gradient of the decision boundary.
c = -b/w2
m = -w1/w2

xmin, xmax = X_train[:, 0].min()-1, X_train[:, 0].max()+1
ymin, ymax = X_train[:, 1].min()-1, X_train[:, 1].max()+1

xd = np.array([xmin, xmax])
yd = m*xd + c
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:red', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='tab:blue', alpha=0.2)

plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train,
            cmap='bwr', label='Training data')
plt.scatter(X_test[:, 0], X_test[:, 1], c=y_test,
            cmap='bwr', marker='x', label='Testing data')
plt.xlabel('X1')
plt.ylabel('X2')
plt.legend()
plt.show()

# predikcija na skupu podataka za testiranje
y_pred = model.predict(X_test)

cm = confusion_matrix(y_test, y_pred)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_pred))
disp.plot()
plt.show()

print(" Tocnost : ", accuracy_score(y_test, y_pred))
print(" Odziv : ", recall_score(y_test, y_pred))
print(" Preciznost : ", precision_score(y_test, y_pred))

for i in range(len(y_test)):
    if y_test[i] == y_pred[i]:
        plt.scatter(X_test[i][0], X_test[i][1], color="green")
    else:
        plt.scatter(X_test[i][0], X_test[i][1], color="black")

plt.xlabel('X1')
plt.ylabel('X2')
plt.show()
